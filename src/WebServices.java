import java.util.Collection;

public class WebServices {
	
}


/*
- Siempre utiliza sustantivos para nombrar tus recursos.
- Añade los nombres en plural para las urls.
- Las modificaciones a recursos deben hacerse con su verbo HTTP correspondiente: POST, PUT o DELETE. - pero con nombres de sustantivos NO VERBOS
- Para devolver recursos asociados a otro recurso utiliza url que incorporen subrecursos: /Autos/1/Choferes.
- Cuando devuelvas colecciones deben ser filtrables, ordenables y paginables.
- Versiona tu API, añade el número de versión en la url: v1/Autos.
 */


 /*
 //How to declare a CRUD urls
HTTP GET http://api.example.com/device-management/managed-devices  //Get all devices
HTTP POST http://api.example.com/device-management/managed-devices  //Create new Device

HTTP GET http://api.example.com/device-management/managed-devices/{id}  //Get device for given Id
HTTP PUT http://api.example.com/device-management/managed-devices/{id}  //Update device for given Id
HTTP DELETE http://api.example.com/device-management/managed-devices/{id}  //Delete device for given Id
*/

/*
//Devolucion de datos
http://api.example.com/device-management/managed-devices.xml  /*Do not use it
http://api.example.com/device-management/managed-devices 	/*This is correct URI
*/

/*
//Do not use trailing forward slash (/) in URIs
http://api.example.com/device-management/managed-devices/ 
http://api.example.com/device-management/managed-devices 	//This is much better version
*/

/**
//Use forward slash (/) to indicate a hierarchical relationships
http://api.example.com/device-management
http://api.example.com/device-management/managed-devices
http://api.example.com/device-management/managed-devices/{id}
http://api.example.com/device-management/managed-devices/{id}/scripts
http://api.example.com/device-management/managed-devices/{id}/scripts/{id}
 */


 /**
 This was in PHP - platzi course - in console mode

 //Para levantar servidor de php en la consola
php -S localhost:8000 file.php

//Para poder probar tu webservice, si funciona correctamente
curl http://localhost:8000/ -v

//Y despues puedes hacer tu consulta normal
http://localhost:8000/?resource_type=books
*/