//Mala practica si solo deseas accedes a pocas clases de ese paquete
import java.awt.*;

//Buena practica
import java.io.IOException;


public class Importaciones {
	//Importaciones
	/*Es una mala costumbre de muchos desarrolladores poner esto:
	java.awt.*; poner el asterisco sin saber el nombre de la clase especifica
	y si solo usaras una clase de ese paquete awt*/
	/*Tambien evitas confuciones ya que hay clases diferentes pero con el mismo
	nombre de metodo y asi es confuso.*/
}
