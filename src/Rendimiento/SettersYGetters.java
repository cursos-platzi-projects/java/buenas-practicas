package Rendimiento;

public class SettersYGetters {
	
	
	//En esta aparto la variable que se asigna de esta manera esta bien pero no seria una buena
	//practica.
	public static void main(String[] arg){
		String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo"};
	}
	
	//La buena practica seria instanciar esta variable como ponerla como privado
	//De esta manera se vuelve mejor el rendimiento y usuaras mas que nada estas variables
	//que casi no cambia mucho de valor como en este caso son los meses.
	private String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo"};
	
	public String[] getMessages(){
		return meses;
	}
	
	public void setMeses(String[] meses){
		this.meses = meses;
	}
}
