package Rendimiento;
//Usar la concatenacion con Buffer con un string e irle sumando.
public class Concatenacion {
	public static void main(String[] arg){
		//La mala practica de concatenacion seria de esta manera.
		//Es una mala practica por que generaras el doble costo de memoria, procesador
		//y eso genera un mal rendimiento al programa
		String cadena = "Uno";
		cadena = cadena + "Dos";
		
		/*Si se desea hacer una concatenacion, seria hacerlo con la clase Buffer y con el metodo
		 append.*/
		//Esto es una buena practica y mejora muchisimo el rendimiento del programa
		StringBuffer sb = new StringBuffer("Uno");
		sb.append("Dos");
	}
}
