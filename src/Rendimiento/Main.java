package Rendimiento;

import java.util.Vector;

public class Main {
	public static void main(String[] arg){
		/*Si en algun momento dado se usara colecciones, es muy bueno que se le asigne
		un espacio de memoria , ejemplo*/
		Vector diasDeLaSemana = new Vector(7);
		
		/*En este caso sabemos que los dias de la semana no cambian y menos los meses
		 asi que usar colecciones para ese tipo de casos vale la pena pero si no sabe
		 el numero de espacio a asignarle es mejor evitarlo y usar otro metodo*/
		
	}
}
