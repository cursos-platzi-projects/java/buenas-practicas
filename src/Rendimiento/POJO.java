package Rendimiento;
//La clase POJO o objeto , se le llama asi a una clase plana extenciones o importanciones.
//Este tipo de clases se usa mucho para poner los setters y getters.
public class POJO {
	int valor;
	String dato;
	
	//Puede tener un constructor
	public POJO(){}
	
	public int getValor(){return valor;}
	public void setValor(int valor){this.valor = valor;}
	
	public String getDato(){return dato;}
	public void setDato(String dato){this.dato = dato;}
}
