package Rendimiento;

public class LiberarRecursos {
	
	public static void main(String[] arg){
		String nombre = "Sara";
		
		if(nombre.equals("Sara")){
			//System.out.print("Se llama Sara");
		}
		
		//De esta manera le estas diciendo a java que quieres liberar memoria,
		//por asi decirlo la variable nombre se vuelve inserbible una vez que el asignas null
		nombre = null;
		
	}
}
