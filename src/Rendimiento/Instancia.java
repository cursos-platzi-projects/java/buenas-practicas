package Rendimiento;

public class Instancia {
	
	public static void main(String[] arg){
		
		//Tipo de instancia
		//Instancia normal es
		String cadena = new String("cadena");
		
		//Para el rendimiento seria de esta forma
		//En este caso estamos asignando un valor entero a un string
		String dato = String.valueOf(10);
		//Para esta instruccion estamos asignandole a un entero una cadena
		int numero = Integer.valueOf("10");
		
	}
}
