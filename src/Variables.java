
public class Variables {
	//Variables
	/*Se le llama inicializar variable a esto int numero = 0; es asignarle un valor.
	Es muy mal gusto declarar variables con nombres: a,e,d,f,g o a1,a2,a3.
	Para las variable privadas se declaran con un _ al principio: "private String _dato;"*/
	
	String cadena = null;
	int numero,edad;
	boolean hombre = false;
	private String _dato;
	
	/*En este codigo es una buena practica de declarar variables en for*/
	public static void main(String[] args){
		 String dato = "dato";
		 int i;
		 for(i = 0; i < 10; i++){
		 //Malo hacer esto
	     //String dato = "dato";
		 }	
	}
	
	//Constantes o variables finales
	/*Siempre las variable constantes se deben de poner en mayuscula, es incorrecto ponerlos
	 en minuscula*/
	final int NUMERO_MAXIMO = 10;
	
}
