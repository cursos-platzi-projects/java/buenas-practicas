package CiclosCondiciones;

public class Variables {
	
	public static void main(String[] arg){
		
		//Esto es mas legible y mejor poner una condicion dentro del ciclo,
		//hasta mejor el rendimiendo del programa si es robusto.
		boolean isCheck = false;
		while(!isCheck){
			
			isCheck = true;
		}
		
		//Esto es valido pero no se ve legible
		while(1 < 10){
			
		}
		
	}
}
