package CiclosCondiciones;

public class UsoParentesis {
	
	public static void main(String[] arg){
		//Las dos formas son validas
		if(10 > 20 || 200 > 1000 
				|| 20 > 1 || 1 < 0){
			
		}
		
		//Pero con esta vista se mejor y mas rapido de entender las condiciones 
		if((10>20)
			|| (200 > 1000)
			|| (20 > 1)
			|| (1>0)){
				
			}
		
	}
}
