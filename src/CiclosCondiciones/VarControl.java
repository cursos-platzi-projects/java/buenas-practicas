package CiclosCondiciones;
//Variables de control
public class VarControl {
	
	
	public static void main(String[] arg){
		
		//Esto lo puedes encontrar habitual pero no es lo correcto
		int i,sum;
		for(i = 0,sum = 0; i<100; i++){
			System.out.println(String.valueOf(i));
		}
		
		//Para que sea mas claro, para saber que la variable i es la mas importante a usar
		sum = 0;
		for(i = 0; i < 100; i++){
			System.out.println(String.valueOf(i));
		}
	}
}
