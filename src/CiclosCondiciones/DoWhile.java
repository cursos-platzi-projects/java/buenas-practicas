package CiclosCondiciones;
//Implementacion del Do-While
public class DoWhile {
	
	public static void main(String[] arg){
		
		//Estructura basica de un do-while
		//Esto es una mala practica para programa robusto y al igual es mejor usarlo sin el do
		int i = 10;
		do{
			
			i = i + 1;
		}while(i > 10);
		
		//Buenas practicas es evitar usar el do, usar lo menos posible de instrucciones.
		
		
	}
}
