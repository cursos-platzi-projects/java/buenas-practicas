package Metodos;

public class NombreComplementario {
	
	//Esto es un metodo complementario el cual hace obtener un valor y devolver ese valor
	public void getNumber(){}
	public void setNumber(){}
	
	//Metodos complementarios para agregar o remover
	public void addNumber(){}
	public void removeNumber(){}
	
	//Para crear y destruir
	public void createData(){}
	public void destroyData(){}
	
	//Para inicio
	public void startWindow(){}
	public void stopWindow(){}
	
	//Insertar y eliminar
	public void insertData(){}
	public void deleteData(){}
	
	//Aveces es buena idea dejarlo del mismo tama�o
	public void insData(){}
	public void dltData(){}
	
	//Incremento y decremento
	public void incrementData(){}
	public void decrementData(){}
	
	//Obtener un numero maximo y un numero minimo
	public void maxNumber(){}
	public void minNumber(){}
	
	//Los habituales
	public void nextData(){}
	public void previousData(){}
	
	//Principales metodos complementarios que se encontraran
	public void showFile(){}
	public void hideFile(){}
	
}
