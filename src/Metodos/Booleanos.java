package Metodos;

public class Booleanos {
	/*Esto es cuando quieres que un metodo te regrese un true o false
	 Siempre poniendo primero is y despues la accion a realizar*/
	
	//Para hacer un metodo como una condicion
	public boolean isOpen(){
		return true; //Si se habrio la puerta
		//return false; //No se pudo habrir la puerta
	}
	
	//Para saber si algo ya termino
	public boolean isFinished(){
		return true; //Si termino algo
		//return false; //No ha terminado algo
	}
}
