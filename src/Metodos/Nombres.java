package Metodos;

public class Nombres {
	/*Los nombres de los metodos deben de llevar el nombre de una accion*/
	public void escribir(){
		
	}
	
	public void calcularGraverdad(){
		
	}
	
	//En el getter y setter se pueden poner nombres en espa�ol pero lo recomendado es en ingles
	public void setNumber(){}
	public void getNumber(){}
	
	public void findData(){}
	public void initializeData(){}
	public void printData(){}
	
}
