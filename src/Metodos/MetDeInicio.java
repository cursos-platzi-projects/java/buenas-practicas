package Metodos;
//Metodos de inicio
public class MetDeInicio {
	int numero;
	
	//Esto es una mala practica al inicializar una clase
	public void initialize(){
		this.numero = 20;
	}
	
	//Es mejor hacerlo con un constructor
	public MetDeInicio(){
		this.numero = 20;
	}
	
	/*Es mejor el constructor ya que si deseas instanciar un objeto, el constructor siempre
	 se va a ejecutar, mientras el metodo "initialize" no y se tendria que llamar a este metodo
	 lo cual ocacionaria agregar una linea mas de codigo*/
}
