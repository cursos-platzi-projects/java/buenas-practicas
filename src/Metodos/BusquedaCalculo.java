package Metodos;

public class BusquedaCalculo {
	
	/*Para asignar nombres a metodos para encontrar numeros*/
	public int findNumber(){
		return 1;
	}
	
	public int findNumberMax(){
		return 10;
	}
	
	/*Para buscar un nombre*/
	public String findName(){
		return "name";
	}
	
	/*Para hacer un calculo en un metodo*/
	public int calculateProm(){
		return 9;
	}
	
	/*Para no usar la palabra calcular, puedes usar la de computar*/
	public int computeProm() {
		return 9;
	}
}
